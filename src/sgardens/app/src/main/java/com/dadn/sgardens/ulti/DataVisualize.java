package com.dadn.sgardens.ulti;

import android.util.Log;

import com.jjoe64.graphview.series.DataPoint;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DataVisualize implements Serializable {
    private static final String TAG = "DataPoint";
    private static final String NUMOFVALUE = "100";

    public String field; //1,2,3,4,5,6

    private int lastEntry;
    private DataPoint lastPoint;

    public DataPoint getLastPoint() {
        if (lastPoint != null)
            return lastPoint;
        else
            return null;
    }

    private DataPoint[] dataBuffer;
    public DataPoint[] getDataBuffer() {
        if (dataBuffer != null)
        return dataBuffer;
        else
            return null;
    }



    public DataVisualize(String field) {
        this.field = field;
        getDataFromThingSpeak();
    }
    public void getDataFromThingSpeak() {
        OkHttpClient okHttpClient = new OkHttpClient();
        Request.Builder builder = new Request.Builder();
        String URL = "https://api.thingspeak.com/channels/1078923/field/" + field + ".json?results=100";
        Request request = builder.url(URL).build(); //&results= ? - Đọc bao nhiêu data

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                //CASE FAIL

            }

            @Override
            public void onResponse(Response response) throws IOException {
                String jsonString = response.body().string();
                try {
                    JSONObject reader = new JSONObject(jsonString);
                    JSONObject channel = reader.getJSONObject("channel");
                    lastEntry = Integer.parseInt(channel.getString("last_entry_id"));
                    JSONArray feeds = reader.getJSONArray("feeds");
                    generateData(feeds);
                } catch (JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());

                }
            }
        });
    }

    private void generateData(JSONArray feeds) throws JSONException {
        List<String> field_data = new ArrayList<String>();
        for (int i = 0; i < feeds.length(); i++) {
            JSONObject entry = feeds.getJSONObject(i);
            String temp = entry.getString("field" + field);
            if (!temp.equals("null")) {
                field_data.add(temp);
            }
        }
        DataPoint[] _field1 = new DataPoint[field_data.size()];
        for (int i = 0; i < field_data.size(); i++) {
            if (isDouble(field_data.get(i))){
                DataPoint v = new DataPoint(i, Double.parseDouble(field_data.get(i)));
                _field1[i] = v;
                if ( i == field_data.size() -1){
                    lastPoint = v;
            }
            }
        }
        this.dataBuffer = _field1;
    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }


}
