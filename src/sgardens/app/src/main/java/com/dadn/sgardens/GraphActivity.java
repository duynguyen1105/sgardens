package com.dadn.sgardens;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.dadn.sgardens.ulti.DataVisualize;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.ValueDependentColor;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.Serializable;

import static com.dadn.sgardens.component.FloorFragment.EXTRA_DATAVISUALIZE;
import static com.dadn.sgardens.component.FloorFragment.EXTRA_FLOOR_ID;
import static com.dadn.sgardens.component.FloorFragment.EXTRA_TYPES;

public class GraphActivity extends Activity {
    private GraphView graph;
    private LineGraphSeries<DataPoint> series = new LineGraphSeries<>();

    /**
     * FloorID :
     * 1. "Floor1"
     * 2. "Floor2"
     */
    private String floorID;
    /**
     * Types:
     * 1. "Temperature"
     * 2. "Humidity"
     * 3. "Windy"
     */
    private String types;
    /**
     * DataVisualize Object
     */
    private DataVisualize data;

    // Android View
    private Toolbar toolbar;
    private TextView title;

    public GraphActivity() {
    }

    public GraphActivity(String floorID, String types, DataVisualize data) {
        this.floorID = floorID;
        this.types = types;
        this.data = data;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph_activity);
        View view = findViewById(R.id.graph_background);
        initActivity();
        title = findViewById(R.id.toolbar_title);
        toolbar = findViewById(R.id.app_bar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        graph = findViewById(R.id.graphView);
        checkFloor(view);
        drawGraph();

    }

    private void checkFloor(View view){
        switch (floorID){
            case "Floor1":
                checkTypes(view, 1);
                break;
            case "Floor2":
                checkTypes(view, 2);
        }

    }

    private void checkTypes(View view, int floor) {
        switch (types) {
            case "Temperature":
                view.setBackgroundResource(R.drawable.sg_temperature_gradient_background_ver);
                title.setText("Nhiệt độ");
                grapViewportTemperature();
                /**
                 * in here we have :
                 * 1. floor_id in varible floor
                 * 2. type is temperature
                 */

                break;
            case "Humidity":
                view.setBackgroundResource(R.drawable.sg_humility_gradient_background_ver);
                title.setText("Độ ẩm");
                grapViewportHumidity();

                break;
            case "Windy":
                view.setBackgroundResource(R.drawable.sg_wind_gradient_background_ver);
                title.setText("Tốc độ gió");
                grapViewportWind();
                break;
            default:
                view.setBackgroundColor(Color.WHITE);
        }
    }


    private void drawGraph() {
        if (data.getDataBuffer() != null){
            series = new LineGraphSeries<>(data.getDataBuffer());
            stylingData();
            graph.addSeries(series);
        }

    }



    private void initActivity() {
        Intent intent = getIntent();
        floorID = intent.getStringExtra(EXTRA_FLOOR_ID);
        types = intent.getStringExtra(EXTRA_TYPES);
        data = (DataVisualize) intent.getSerializableExtra(EXTRA_DATAVISUALIZE);
    }


    protected void stylingData() {
        // styling
        series.setColor(Color.GREEN);
        series.setDrawDataPoints(true);
        series.setDataPointsRadius(10);
        series.setThickness(8);
        series.setDrawBackground(true);
        series.setDrawAsPath(true);


// custom paint to make a dotted line

    }

    protected void grapViewportTemperature(){
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(60);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(20);
        graph.getViewport().setScrollable(true);
    }

    protected void grapViewportHumidity(){
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(100);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(20);
        graph.getViewport().setScrollable(true);
    }

    protected void grapViewportWind(){
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(0);
        graph.getViewport().setMaxY(150);

        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(20);
        graph.getViewport().setScrollable(true);
    }

}
