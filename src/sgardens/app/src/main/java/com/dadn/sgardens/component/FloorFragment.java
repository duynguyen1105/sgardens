package com.dadn.sgardens.component;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dadn.sgardens.DetailActivity;
import com.dadn.sgardens.GraphActivity;
import com.dadn.sgardens.ulti.DataVisualize;
import com.dadn.sgardens.ulti.ProductCardRecyclerViewAdapter;
import com.dadn.sgardens.ulti.ProductGridItemDecoration;
import com.dadn.sgardens.R;
import com.dadn.sgardens.network.ProductEntry;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.snackbar.Snackbar;
import com.jjoe64.graphview.series.DataPoint;

import java.util.Timer;
import java.util.TimerTask;

public class FloorFragment extends Fragment implements ProductCardRecyclerViewAdapter.OnItemClickListener {
    private static final String TAG = "Floor Fragment";
    private String floorID;

    public static final String EXTRA_URL = "imageUrl";
    public static final String EXTRA_TITLE = "TitleName";
    public static final String EXTRA_SUBTITLE = "Subtitle";

    public static final String EXTRA_FLOOR_ID = "FloorID";
    public static final String EXTRA_TYPES = "Types";
    public static final String EXTRA_DATAVISUALIZE = "Data_object";


    private ProductCardRecyclerViewAdapter adapter;
    private MaterialCardView temperatureCard;
    private MaterialCardView humidityCard;
    private MaterialCardView windyCard;

    private TextView temperatureText;
    private TextView humidityText;
    private TextView windyText;



    private Timer aTimer = new Timer();
    private DataVisualize temperatureData;
    private DataVisualize humidityData;
    private DataVisualize windyData;



    public FloorFragment(String floorID) {
        this.floorID = floorID;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment with the ProductGrid theme
        View view = inflater.inflate(R.layout.floor_fragment, container, false);
        view.findViewById(R.id.scrollView).setVerticalScrollBarEnabled(false);
        // Set up the RecyclerView
        recyclerViewInit(view);

        /**
         * IOT PARAMETER
         *
         */
        // Text
        temperatureText = view.findViewById(R.id.temperature_text);
        humidityText = view.findViewById(R.id.humidity_text);
        windyText = view.findViewById(R.id.windy_text);
        //DataVisualize
        choiseThingSpeakFields();

        //Card ClickListener
        temperatureCard = view.findViewById(R.id.temperature_card);
        temperatureCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Temperature", Snackbar.LENGTH_SHORT).show();
                goToGraphView(floorID,"Temperature", temperatureData);

            }
        });

        humidityCard = view.findViewById(R.id.humidity_card);
        humidityCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Humidity", Snackbar.LENGTH_SHORT).show();
                goToGraphView(floorID,"Humidity", humidityData);
            }
        });

        windyCard = view.findViewById(R.id.windy_card);
        windyCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Windy", Snackbar.LENGTH_SHORT).show();
                goToGraphView(floorID,"Windy",windyData);
            }
        });

        setupTimer(1000);
        return view;
    }


    public void setupTimer(int value) {
        TimerTask readLastPoint = new TimerTask() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                if (temperatureData != null){
                    temperatureData.getDataFromThingSpeak();
                    if (temperatureData.getLastPoint() != null){
                        temperatureText.setText((int) temperatureData.getLastPoint().getY() + " \u2103");
                    }
                }
                if (humidityData != null){
                    humidityData.getDataFromThingSpeak();
                    if (humidityData.getLastPoint() != null){
                        humidityText.setText((int) humidityData.getLastPoint().getY() + " %");
                    }
                }
                if (windyData != null){
                    windyData.getDataFromThingSpeak();
                    if (windyData.getLastPoint() != null){
                        windyText.setText((int) windyData.getLastPoint().getY() + " m/s");
                    }
                }
            }
        };
        if (value != 0){
            aTimer.schedule(readLastPoint, 100, value);
        } else{
            aTimer.cancel();
        }

    }

    private void choiseThingSpeakFields(){
        switch (floorID) {
            case "Floor1":
                temperatureData = new DataVisualize("4");
                humidityData = new DataVisualize("5");
                windyData = new DataVisualize("3");
                break;
            case "Floor2":
                temperatureData = new DataVisualize("4");
                humidityData = new DataVisualize("5");
                windyData = new DataVisualize("3");
                break;
            default:
                break;
        }

    }

    public void goToGraphView(String floorID, String types,DataVisualize data){
        Intent graphIntent = new Intent(getActivity().getBaseContext(), GraphActivity.class);

        graphIntent.putExtra(EXTRA_FLOOR_ID, floorID);
        graphIntent.putExtra(EXTRA_TYPES, types);
        graphIntent.putExtra(EXTRA_DATAVISUALIZE, data);

        startActivity(graphIntent);
    }

    @Override
    public void onItemClick(int position) {
        Intent detailIntent = new Intent(getActivity().getBaseContext(), DetailActivity.class);
        ProductEntry product = adapter.getProductList().get(position);

        detailIntent.putExtra(EXTRA_URL, product.url);
        detailIntent.putExtra(EXTRA_TITLE, product.title);
        detailIntent.putExtra(EXTRA_SUBTITLE, product.descriptions);

        startActivity(detailIntent);
    }

    public void recyclerViewInit(View view){
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        adapter = new ProductCardRecyclerViewAdapter(
                ProductEntry.initProductEntryList(getResources(), floorID), getActivity());
        adapter.setOnItemClickListener(this);
        recyclerView.setAdapter(adapter);
        int largePadding = getResources().getDimensionPixelSize(R.dimen.product_grid_spacing);
        int smallPadding = getResources().getDimensionPixelSize(R.dimen.product_grid_spacing_small);
        recyclerView.addItemDecoration(new ProductGridItemDecoration(largePadding, smallPadding));
    }
}
