package com.dadn.sgardens.component;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.dadn.sgardens.R;
import com.google.android.material.tabs.TabLayout;

public class HomeFragment extends Fragment  {
    TabLayout tabLayout;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment with the ProductGrid theme
        View view = inflater.inflate(R.layout.home_fragment, container, false);
        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch(tab.getPosition()){
                    case 0:
                        loadFragment(new FloorFragment("Floor1"));
                        //ft.commit();
                        break;
                    case 1:
                        loadFragment(new FloorFragment("Floor2"));
                        //ft.commit();
                        break;
                    default:
                        Log.e("ERROR","ERROR");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        loadFragment(new FloorFragment("Floor1"));
        return view;
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.floor_fragment_holder, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
