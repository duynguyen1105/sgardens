package com.dadn.sgardens.component;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.dadn.sgardens.ulti.MQTTHelper;
import com.dadn.sgardens.R;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONArray;
import org.json.JSONObject;
public class ConfigFragment extends Fragment {
    private static final String TAG = "MQTT_Debug";
    private MQTTHelper bkMQTT, myMQTT;

    private final String LightSub = "Light";
    private final String LightPub = "LightD";


    com.shawnlin.numberpicker.NumberPicker picker1, picker2;
    Timer timer;
    Switch switch1, switch2, switchserver;
    SeekBar seek1, seek2;
    TextView light1, light2;


    int seekval1 = 0;
    int seekval2 = 0;
    private final byte seekvalMin = 0;
    private final byte timerValue = 10;

    private final int numberPickerStep = 100;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        bkMQTT = new MQTTHelper(Objects.requireNonNull(getActivity()).getApplicationContext(), MQTTHelper.BK_MQTT_BROKER);
        myMQTT = new MQTTHelper(Objects.requireNonNull(getActivity()).getApplicationContext(), MQTTHelper.MY_MQTT_BROKER);
        startmyMQTTSUB();
        startmyMQTT();
    }

    private void setupView(View view) {
        picker1 = view.findViewById(R.id.number_picker1);
        picker2 = view.findViewById(R.id.number_picker2);
        seek1 = view.findViewById(R.id.seekBar1);
        seek2 = view.findViewById(R.id.seekBar2);
        light1 = view.findViewById(R.id.textView2);
        light2 = view.findViewById(R.id.textView8);
        switch1 = view.findViewById(R.id.switch1);
        switch2 = view.findViewById(R.id.switch2);
        switchserver = view.findViewById(R.id.switchserver);
    }


    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment with the ProductGrid theme
        View view = inflater.inflate(R.layout.config_fragment, container, false);
        setupView(view);
        setupStyleView();


        //Manual config
        numberPickerListener();
        seekBarListener();
        switchListener();


        // Auto Config
        serverListener();


        return view;
    }

    private void serverListener() {
        switchserver.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String value = isChecked ? "1" : "0";
                MqttMessage msg = new MqttMessage();
                msg.setId(1234);
                msg.setQos(1);
                msg.setRetained(true);
                byte[] b = value.getBytes(StandardCharsets.UTF_8);
                msg.setPayload(b);
                try {
                    myMQTT.mqttAndroidClient.publish("Topic/Auto", msg);

                } catch (MqttException ignored) {
                }
            }
        });
    }

    private void switchListener() {
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                JsonArray value = new JsonArray();
                value.add(isChecked ? "1" : "0");
                value.add(Integer.toString(picker1.getValue() * numberPickerStep));
                sendDataToMQTT(myMQTT, "Speaker", value, "Topic/Speaker");
                timer.cancel();
                timer = new Timer();
                timer.schedule(new turnOFFAndResetNumberPicker(), timerValue * 1000);
            }
        });
        switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                JsonArray value = new JsonArray();
                value.add(isChecked ? "1" : "0");
                value.add(Integer.toString(picker2.getValue() * numberPickerStep));

                sendDataToMQTT(bkMQTT, "Speaker", value, "Topic/Speaker");
                timer.cancel();
                timer = new Timer();
                timer.schedule(new turnOFFAndResetNumberPicker(), timerValue * 1000);
            }
        });
    }


    private void seekBarListener() {
        seek1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekval1 = progress + seekvalMin;
                light1.setText("" + seekval1);
                if (seekval1 == 0) light1.setText("off");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("Debug", String.valueOf(seekBar.getProgress()));
                seekval1 = seekBar.getProgress();
                boolean checked = seekval1 == 0;
                JsonArray value = new JsonArray();
                value.add(!checked ? "1" : "0");
                value.add(Integer.toString(seekval1));
                Log.d("Value", value.toString());
                sendDataToMQTT(bkMQTT, LightPub, value, "Topic/LightD");
            }
        });
        seek2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                seekval2 = progress + seekvalMin;
                light2.setText("" + seekval2);
                if (seekval2 == 0) light2.setText("off");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("Debug", "Start" );
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.d("Debug", String.valueOf(seekBar.getProgress()));
                seekval2 = seekBar.getProgress();
                boolean checked = seekval2 == 0;
                JsonArray value = new JsonArray();
                value.add(!checked ? "1" : "0");
                value.add(Integer.toString(seekval2));
                Log.d("Value", value.toString());
                sendDataToMQTT(bkMQTT, LightPub, value, "Topic/LightD");
            }
        });
    }

    private void numberPickerListener() {
        picker1.setOnValueChangedListener(new com.shawnlin.numberpicker.NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(com.shawnlin.numberpicker.NumberPicker picker, int oldVal, int newVal) {
                timer.cancel();
                JsonArray value = new JsonArray();
                value.add(switch1.isChecked() ? "1" : "0");
                value.add(Integer.toString(picker1.getValue() * numberPickerStep));
                sendDataToMQTT(bkMQTT, "Speaker", value, "Topic/Speaker");
                timer = new Timer();
                timer.schedule(new turnOFFAndResetNumberPicker(), timerValue * 1000);
            }
        });
        picker2.setOnValueChangedListener(new com.shawnlin.numberpicker.NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(com.shawnlin.numberpicker.NumberPicker picker, int oldVal, int newVal) {
                timer.cancel();
                JsonArray value = new JsonArray();
                value.add(switch1.isChecked() ? "1" : "0");
                value.add(Integer.toString(picker2.getValue() * numberPickerStep));
                sendDataToMQTT(bkMQTT, "Speaker", value, "Topic/Speaker");
                timer = new Timer();
                timer.schedule(new turnOFFAndResetNumberPicker(), timerValue * 1000);
            }
        });
    }

    class turnOFFAndResetNumberPicker extends TimerTask {
        @Override
        public void run() {
            JsonArray value = new JsonArray();
            value.add("0");
            value.add("0");
            sendDataToMQTT(bkMQTT, "Speaker", value, "Topic/Speaker");
        }
    }

    private void sendDataToMQTT(MQTTHelper server, String ID, JsonArray value, String TS) {

        MqttMessage msg = new MqttMessage();
        msg.setId(1234);
        msg.setQos(1);
        msg.setRetained(true);
        JsonArray jsonArray = new JsonArray();
        JsonObject data = new JsonObject();
        data.addProperty("device_id", ID);
        data.add("values", value);
        jsonArray.add(data);
        byte[] b = jsonArray.toString().getBytes(StandardCharsets.UTF_8);
        Log.d("Value", jsonArray.toString());
        msg.setPayload(b);

        try {
            server.mqttAndroidClient.publish(TS, msg);

        } catch (MqttException ignored) {
        }
    }

    private void startmyMQTT() {
        myMQTT.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                try {
                    Log.w(TAG, String.valueOf(message));
                } catch (Exception e) {
                    throw (e);
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

    }

    private void startmyMQTTSUB() {
        bkMQTT.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean b, String s) {
                Log.w("Connect", "Connection!  ");
            }

            @Override
            public void connectionLost(Throwable throwable) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                Log.w(TAG, mqttMessage.toString());
                try {
                    JSONArray array=new JSONArray(mqttMessage.toString());
                    JSONObject object=array.getJSONObject(0);
                    String device_name=object.getString("device_id");
                    JSONArray values=object.getJSONArray("values");
                    int first_value=Integer.parseInt(values.get(0).toString());
                    int second_value=Integer.parseInt(values.get(1).toString());
                    if (device_name.equals(LightSub)) {
                        if (first_value == 0 || second_value == 0) {
                            seek1.setProgress(0);
                            seek2.setProgress(0);
                            light1.setText("off");
                            light2.setText("off");
                            return;
                        }
                        light1.setText("" + second_value);
                        light2.setText("" + second_value);
                        seek1.setProgress(second_value);
                        seek2.setProgress(second_value);
                    } else if(device_name.equals("Speaker")) {
                        picker2.setValue(second_value / numberPickerStep);
                        picker1.setValue(second_value / numberPickerStep);
                        if (first_value == 0) {
                            switch1.setChecked(false);
                            switch2.setChecked(false);
                            return;
                        }
                        switch1.setChecked(true);
                        switch2.setChecked(true);

                    } else{
                        return;
                    }
                } catch(Exception e){
                    Log.w("Error",e.toString());
                }
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void setupStyleView() {
        light1.setText("off");
        light2.setText("off");
        int seekvalMax = 255;
        seek1.setMax(seekvalMax);
        seek2.setMax(seekvalMax);
        int minValue = 0;
        int maxValue = 50;

        String[] numberValues = new String[maxValue - minValue + 1];
        for (int i = 0; i <= maxValue - minValue; i++) {
            numberValues[i] = String.valueOf((minValue + i) * numberPickerStep);
        }

        picker1.setMinValue(minValue);
        picker1.setMaxValue(maxValue);

        picker1.setWrapSelectorWheel(false);
        picker1.setDisplayedValues(numberValues);
        picker2.setMinValue(minValue);
        picker2.setMaxValue(maxValue);

        picker2.setWrapSelectorWheel(false);
        picker2.setDisplayedValues(numberValues);
        timer = new Timer();
        // Set divider color
        picker1.setDividerColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorPicker));
        picker1.setDividerColorResource(R.color.colorPicker);
        picker2.setDividerColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorPicker));
        picker2.setDividerColorResource(R.color.colorPicker);
        // Set formatter
        picker1.setSelectedTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorPicker));
        picker1.setSelectedTextColorResource(R.color.colorPicker);
        picker2.setSelectedTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.colorPicker));
        picker2.setSelectedTextColorResource(R.color.colorPicker);

// Set selected text size
        picker1.setSelectedTextSize(50.f);
        picker2.setSelectedTextSize(50.f);

// Set selected typeface
        picker1.setSelectedTypeface(Typeface.create(getString(R.string.roboto_light), Typeface.NORMAL));
        picker1.setSelectedTypeface(getString(R.string.roboto_light), Typeface.NORMAL);
        picker1.setSelectedTypeface(getString(R.string.roboto_light));
        picker1.setSelectedTypeface(R.string.roboto_light, Typeface.NORMAL);
        picker1.setSelectedTypeface(R.string.roboto_light);
        picker2.setSelectedTypeface(Typeface.create(getString(R.string.roboto_light), Typeface.NORMAL));
        picker2.setSelectedTypeface(getString(R.string.roboto_light), Typeface.NORMAL);
        picker2.setSelectedTypeface(getString(R.string.roboto_light));
        picker2.setSelectedTypeface(R.string.roboto_light, Typeface.NORMAL);
        picker2.setSelectedTypeface(R.string.roboto_light);

// Set text color
        picker1.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.dark_grey));
        picker1.setTextColorResource(R.color.dark_grey);
        picker2.setTextColor(ContextCompat.getColor(Objects.requireNonNull(getActivity()), R.color.dark_grey));
        picker2.setTextColorResource(R.color.dark_grey);

// Set text size
        picker1.setTextSize(50.f);
        picker2.setTextSize(50.f);

// Set typeface
        picker1.setTypeface(Typeface.create(getString(R.string.roboto_light), Typeface.NORMAL));
        picker1.setTypeface(getString(R.string.roboto_light), Typeface.NORMAL);
        picker1.setTypeface(getString(R.string.roboto_light));
        picker1.setTypeface(R.string.roboto_light, Typeface.NORMAL);
        picker1.setTypeface(R.string.roboto_light);
        picker2.setTypeface(Typeface.create(getString(R.string.roboto_light), Typeface.NORMAL));
        picker2.setTypeface(getString(R.string.roboto_light), Typeface.NORMAL);
        picker2.setTypeface(getString(R.string.roboto_light));
        picker2.setTypeface(R.string.roboto_light, Typeface.NORMAL);
        picker2.setTypeface(R.string.roboto_light);
    }

}
